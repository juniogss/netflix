package com.junio.netflix.controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.junio.netflix.databinding.ActivityMainBinding;
import com.junio.netflix.databinding.CategoryItemBinding;
import com.junio.netflix.databinding.MovieItemBinding;
import com.junio.netflix.model.Category;
import com.junio.netflix.model.Movie;
import com.junio.netflix.util.CategoryTask;
import com.junio.netflix.util.ImageDownloaderTask;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CategoryTask.CategoryLoader {

    private MainAdapter mainAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        List<Category> categories = new ArrayList<>();

        mainAdapter = new MainAdapter(categories);
        binding.rvMain.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.rvMain.setAdapter(mainAdapter);

        CategoryTask categoryTask = new CategoryTask(this);
        categoryTask.setCategoryLoader(this);
        categoryTask.execute("https://tiagoaguiar.co/api/netflix/home");
    }

    @Override
    public void onResult(List<Category> categories) {
        mainAdapter.setCategories(categories);
        mainAdapter.notifyDataSetChanged();
    }

    private static class CategoryHolder extends RecyclerView.ViewHolder {

        public final TextView tvTitle;
        public final RecyclerView rvMovie;

        public CategoryHolder(@NonNull CategoryItemBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            rvMovie = binding.rvMovie;
        }
    }

    private static class MovieHolder extends RecyclerView.ViewHolder {

        public final ImageView ivCover;

        public MovieHolder(@NonNull MovieItemBinding binding, final OnItemClickListener onItemClickListener) {
            super(binding.getRoot());
            ivCover = binding.ivCover;

            binding.getRoot().setOnClickListener(v -> onItemClickListener.onClick(getAdapterPosition()));

        }
    }

    private class MainAdapter extends RecyclerView.Adapter<CategoryHolder> {

        private final List<Category> categories;

        private MainAdapter(List<Category> categories) {
            this.categories = categories;
        }

        @NonNull
        @Override
        public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            CategoryItemBinding binding = CategoryItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new CategoryHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
            Category category = categories.get(position);
            holder.tvTitle.setText(category.getName());
            holder.rvMovie.setAdapter(new MovieAdapter(category.getMovies()));
            holder.rvMovie.setLayoutManager(new LinearLayoutManager(getBaseContext(), RecyclerView.HORIZONTAL, false));
        }

        @Override
        public int getItemCount() {
            return categories.size();
        }

        public void setCategories(List<Category> categories) {
            this.categories.clear();
            this.categories.addAll(categories);
        }
    }

    private class MovieAdapter extends RecyclerView.Adapter<MovieHolder> implements OnItemClickListener {

        private final List<Movie> movies;

        private MovieAdapter(List<Movie> movies) {
            this.movies = movies;
        }

        @NonNull
        @Override
        public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            MovieItemBinding binding = MovieItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new MovieHolder(binding, this);
        }

        @Override
        public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
            Movie movie = movies.get(position);
            new ImageDownloaderTask(holder.ivCover).execute(movie.getCoverUrl());

//            holder.ivCover.setImageResource(movie.getCoverUrl());
        }

        @Override
        public int getItemCount() {
            return movies.size();
        }

        @Override
        public void onClick(int position) {
            if (movies.get(position).getId() <= 3) {
                Intent intent = new Intent(MainActivity.this, MovieActivity.class);
                intent.putExtra("id", movies.get(position).getId());
                startActivity(intent);
            }
        }
    }

    public interface OnItemClickListener {
        void onClick(int position);
    }
}