package com.junio.netflix.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.junio.netflix.model.Movie;
import com.junio.netflix.model.MovieDetail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Junio Gabriel on 06/06.
 */
public class MovieDetailTask extends AsyncTask<String, Void, MovieDetail> {

    private final WeakReference<Context> context;
    private MovieDetailLoader movieDetailLoader;
    private ProgressDialog dialog;

    public MovieDetailTask(Context context) {
        this.context = new WeakReference<>(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Context context = this.context.get();
        if (context != null)
            dialog = ProgressDialog.show(context, "Carregando", "", true);
    }

    @Override
    protected MovieDetail doInBackground(String... params) {
        String url = params[0];
        try {
            URL urlRequest = new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection) urlRequest.openConnection();
            urlConnection.setReadTimeout(2000);
            urlConnection.setConnectTimeout(2000);

            int respondeCode = urlConnection.getResponseCode();
            if (respondeCode > 400) {
                throw new IOException("Erro na comunicação com o servidor");
            }

            InputStream inputStream = urlConnection.getInputStream();

            BufferedInputStream in = new BufferedInputStream(inputStream);

            String jsonAsString = toString(in);

            MovieDetail movieDetail = getMovieDetail(new JSONObject(jsonAsString));

            in.close();
            return movieDetail;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private MovieDetail getMovieDetail(JSONObject json) throws JSONException {

        int id = json.getInt("id");
        String title = json.getString("title");
        String synopsis = json.getString("desc");
        String cast = json.getString("cast");
        String coverUrl = json.getString("cover_url");

        List<Movie> movies = new ArrayList<>();
        JSONArray movieArray = json.getJSONArray("movie");
        for (int i = 0; i < movieArray.length(); i++) {
            JSONObject movie = movieArray.getJSONObject(i);
            int similarId = movie.getInt("id");
            String cover_url = movie.getString("cover_url");

            Movie similar = new Movie();
            similar.setId(similarId);
            similar.setCoverUrl(cover_url);
            movies.add(similar);
        }

        Movie movie = new Movie();
        movie.setId(id);
        movie.setTitle(title);
        movie.setSynopsis(synopsis);
        movie.setCast(cast);
        movie.setCoverUrl(coverUrl);

        return new MovieDetail(movie, movies);
    }

    @Override
    protected void onPostExecute(MovieDetail movieDetail) {
        super.onPostExecute(movieDetail);
        dialog.dismiss();
        if (movieDetailLoader != null)
            movieDetailLoader.onResult(movieDetail);
    }

    private String toString(InputStream is) throws IOException {
        byte[] bytes = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int lidos;
        while ((lidos = is.read(bytes)) > 0) {
            baos.write(bytes, 0, lidos);
        }
        return new String(baos.toByteArray());
    }

    public void setMovieDetailLoader(MovieDetailLoader movieDetailLoader) {
        this.movieDetailLoader = movieDetailLoader;
    }

    public interface MovieDetailLoader {
        void onResult(MovieDetail movieDetail);
    }
}
