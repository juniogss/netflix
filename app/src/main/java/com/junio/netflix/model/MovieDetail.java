package com.junio.netflix.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Junio Gabriel on 06/06.
 */

@Data
@AllArgsConstructor
public class MovieDetail {

    private Movie movie;
    private List<Movie> moviesSimilar;
}
