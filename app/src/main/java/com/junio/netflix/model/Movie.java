package com.junio.netflix.model;

import lombok.Data;

@Data
public class Movie {

    private int id;
    private String coverUrl;
    private String title;
    private String synopsis;
    private String cast;
}
